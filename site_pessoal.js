var http = require('http')
var fs = require('fs')
var port = 3000

var server = http.createServer(function(request, response){


  if(request.url == "/"){

    // A constante __dirname retorna o diretório raiz da aplicação.
    fs.readFile(__dirname + '/view/index.html', function(err, html){

      response.writeHeader(200, {'Content-Type': 'text/html'})
      response.end(html)

    })

  }else if(request.url == "/artigo"){

    // A constante __dirname retorna o diretório raiz da aplicação.
    fs.readFile(__dirname + '/view/artigo.html', function(err, html){

      response.writeHeader(200, {'Content-Type': 'text/html'})
      response.end(html)

    })

  }else if(request.url == "/contato"){

    // A constante __dirname retorna o diretório raiz da aplicação.
    fs.readFile(__dirname + '/view/contato.html', function(err, html){

      response.writeHeader(200, {'Content-Type': 'text/html'})
      response.end(html)

    })

  }else{

    fs.readFile(__dirname + '/view/error.html', function(err, html){
      response.writeHeader(200, {'Content-Type': 'text/html'})      
      response.end(html)

    })

  }




})

server.listen(port, response => console.log('Servidor rodando na porta ' + port) )
